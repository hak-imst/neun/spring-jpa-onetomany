# Spring JPA OneToMany

#####Wichtigste Annotationen zur 1:N Beziehung in JPA

Comment Klasse:
@ManyToOne(fetch = FetchType.LAZY, optional = false)

@JoinColumn(name = "post_id", nullable = false)

@OnDelete(action = OnDeleteAction.CASCADE)
